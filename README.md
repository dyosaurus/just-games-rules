Just Games Rules


1. Bigotry, hate speech, and intolerance for others' personal and subjective beliefs are strictly prohibited and grounds for an immediate (zero warning) permanent ban. This includes racism, sexism, and other forms of prejudice discrimination. Jokes of this nature or to such an end are equally impermissible.

   - Probing questions and/or a single warning may be issued if there is judged to be reasonable room for alternate (incriminating) interpretation.


2. Exhibit and exude respect toward all current and former members. Personal attacks are strictly prohibited.

   - The content and behavior presented in members' messages and other interactions constitute an acceptable basis for constructive criticism, as long as it does not rise to the level of outright toxicity.

   - Harmless banter in moderation is acceptable, as long as it does not come at the sacrifice of respect. This necessarily means a higher standard of professionalism is expected toward newer or less active members.

3. Linking, uploading, or otherwise directly facilitating access to NSFW content is strictly prohibited.

4. Stay on the topic of the room.

5. Don't send images as your first message.